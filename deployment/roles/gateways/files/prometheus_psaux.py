#!/usr/env/bin python3
import subprocess
from prometheus_client import start_http_server, Summary, Gauge
import time

cpu_usage = Gauge('psaux_cpu_usage', 'CPU usage', ['process', 'pid'])
mem_usage = Gauge('psaux_memory_usage', 'memory usage', ['process', 'pid'])

def process_request():
    #run ps aux
    ret = subprocess.run(["ps", "aux"], stdout=subprocess.PIPE)
    #parse each line of stdout
    for s in ret.stdout.decode('UTF-8').splitlines()[2:]:
        l = s.split()
        #generate strings to push to prometheus
        cpu_usage.labels(process=str(l[10]), pid=str(l[1])).set(float(l[2]))
        mem_usage.labels(process=str(l[10]), pid=str(l[1])).set(float(l[3]))

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8106)
    # Generate some requests.
    while True:
        process_request()
        time.sleep(1)
