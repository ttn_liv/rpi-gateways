from prometheus_client import start_http_server, Gauge
import bme680
import time

sensor = bme680.BME680()

sensor.set_humidity_oversample(bme680.OS_2X)
sensor.set_pressure_oversample(bme680.OS_4X)
sensor.set_temperature_oversample(bme680.OS_8X)
sensor.set_filter(bme680.FILTER_SIZE_3)

sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)
sensor.set_gas_heater_temperature(320)
sensor.set_gas_heater_duration(150)
sensor.select_gas_heater_profile(0)

# Set gauges for the metrics
temperature = Gauge('bme680_temperature', 'Air temperature')
pressure = Gauge('bme680_pressure', 'Air pressure')
humidity = Gauge('bme680_humidity', 'Case humidity')

def process_request():
    """Take and store the values"""
    if sensor.get_sensor_data():
        temperature.set(sensor.data.temperature)
        pressure.set(sensor.data.pressure)
        humidity.set(sensor.data.humidity)

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8105)
    # Generate some requests.
    while True:
        process_request()
        time.sleep(1)
